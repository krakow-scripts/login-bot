# login-bot

Automation of user activity

## Reqirements:

python 2.7

phantomjs 2.0

A file *data.csv* must be located in the same directory as the script itself.

### data.csv

has to look like this:

```
usr,pass
username1,userpass1
username2,userpass2
```

# Usage

1. Clone
1. create **data.csv** as described
1. enter path variable

Run script