#!/usr/bin/python
# coding: utf-8
# Autologin script for multiple accounts

import datetime
import sys  # sys.stdout.write('.')
import traceback
import validators
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support import expected_conditions as EC

import csv

phantom = webdriver.PhantomJS()
path = ''
logfile = open(path + 'login-bot.log', 'a')
logfile.write('['+datetime.datetime.now().strftime("%Y-%m-%d %H:%M")+']: run login-bot\n')

forumurl = 'http://holerajasna.liverolka.ru/login.php'

def loginbot(u, p):
    logfile.write(u + ':')
    phantom.get(forumurl)
    try:
        username = WebDriverWait(phantom,10).until(EC.presence_of_element_located((By.XPATH,'//input[@size = "25"]')))
        password = WebDriverWait(phantom,10).until(EC.presence_of_element_located((By.XPATH,'//input[@size = "16"]')))
        username.send_keys(u.decode('utf-8'))
        password.send_keys(p.decode('utf-8'), Keys.RETURN)
        phantom.implicitly_wait(5)
    except Exception:
        logfile.write('\tERROR on login\n')

def logout():
    try:
        phantom.implicitly_wait(10)
        logout = WebDriverWait(phantom,10).until(EC.presence_of_element_located((By.XPATH,'//li[@id="navlogout"]/a/img')))
        logout.click()
        logfile.write('\tOK\n')
    except Exception:
        logfile.write('\tERROR on logout\n')

# def geturl(line):  # simple url searcher
#         res = line.split('"')
#         for r in res:
#             if validators.url(r):
#                 return r.replace('&amp;', '&')
#         return ''

def main():
    try:
        with open(path + 'data.csv', 'rU') as data:
            reader = csv.DictReader(data)
            for row in reader:
                loginbot(row['usr'], row['pass'])
                logout()
        phantom.quit()
    except KeyboardInterrupt:
        print '\tShutdown requested...exiting'
    except Exception:
        traceback.print_exc(file=sys.stdout)
    sys.exit(0)

if __name__ == "__main__":
    main()
    try:
        phantom.quit()
    except Exception:
        pass
